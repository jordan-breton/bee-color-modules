<?php
namespace wfw\modules\BeeColor\contact\domain\errors;

/**
 * Une erreur d'archivage ou de désarchivage de prise de contact est survenue
 */
final class ArchivingFailure extends ContactFailure{}