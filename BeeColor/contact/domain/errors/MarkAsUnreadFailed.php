<?php
namespace wfw\modules\BeeColor\contact\domain\errors;

/**
 * Impossible de marquer la prise de contact comme "non lue"
 */
final class MarkAsUnreadFailed extends ContactFailure {}