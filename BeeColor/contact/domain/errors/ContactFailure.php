<?php
namespace wfw\modules\BeeColor\contact\domain\errors;

/**
 * Erreur sur une manipulationd 'un agrégat de type contact
 */
class ContactFailure extends \Exception{}