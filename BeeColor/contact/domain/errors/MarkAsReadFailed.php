<?php
namespace wfw\modules\BeeColor\contact\domain\errors;

/**
 * Impossible de marquer la prise de contact comme lue
 */
final class MarkAsReadFailed extends ContactFailure {}