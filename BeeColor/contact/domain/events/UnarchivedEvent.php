<?php
namespace wfw\modules\BeeColor\contact\domain\events;

/**
 * La prise de contact est désarchivée
 */
final class UnarchivedEvent extends ContactEvent {}