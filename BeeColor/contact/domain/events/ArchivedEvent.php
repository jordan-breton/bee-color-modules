<?php
namespace wfw\modules\BeeColor\contact\domain\events;

/**
 * La prise de contact a été archivée.
 */
final class ArchivedEvent extends ContactEvent {}