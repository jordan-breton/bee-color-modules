<?php
namespace wfw\modules\BeeColor\contact\handlers\action;

use wfw\engine\core\command\ICommand;
use wfw\engine\core\command\ICommandBus;
use wfw\engine\core\domain\events\IDomainEvent;
use wfw\engine\core\domain\events\IDomainEventListener;
use wfw\engine\core\domain\events\IDomainEventObserver;
use wfw\engine\core\lang\ITranslator;
use wfw\engine\core\response\IResponse;
use wfw\engine\core\response\responses\Response;
use wfw\engine\core\session\ISession;
use wfw\engine\lib\data\string\json\IJSONEncoder;
use wfw\modules\BeeColor\contact\command\UnarchiveContacts;
use wfw\modules\BeeColor\contact\security\data\ContactIdListRule;
use wfw\modules\BeeColor\contact\domain\events\UnarchivedEvent;

/**
 * Permet d'archiver une liste de prises de contact
 */
final class UnarchiveHandler extends DefaultContactActionHandler implements IDomainEventListener{
	/** @var IJSONEncoder $_encoder */
	private $_encoder;
	/** @var array $_ids */
	private $_ids;

	/**
	 * ArchiveHandler constructor.
	 *
	 * @param ICommandBus          $bus      Bus de commandes
	 * @param ISession             $session  Session
	 * @param ContactIdListRule    $rule     Régle de validation des données
	 * @param IDomainEventObserver $observer Observeur de DomainEventListeners
	 * @param IJSONEncoder         $encoder  Encodeur JSON
	 * @param ITranslator          $translator
	 */
	public function __construct(
		ICommandBus $bus,
		ISession $session,
		ContactIdListRule $rule,
		IDomainEventObserver $observer,
		IJSONEncoder $encoder,
		ITranslator $translator
	) {
		parent::__construct($bus, $rule, $session,$translator);
		$this->_ids = [];
		$this->_encoder = $encoder;
		$observer->addDomainEventListener(UnarchivedEvent::class, $this);
	}

	/**
	 * @param array $data
	 * @return ICommand
	 */
	protected function createCommand(array $data): ICommand {
		return new UnarchiveContacts($this->_session->get('user')->getId(),...$data["ids"]);
	}

	/**
	 * Méthode appelée lors de la reception d'un événement
	 *
	 * @param IDomainEvent $e Evenement reçu
	 */
	public function recieveDomainEvent(IDomainEvent $e): void {
		if($e instanceof UnarchivedEvent) $this->_ids[] = (string) $e->getAggregateId();
	}

	/**
	 * @return IResponse
	 */
	protected function successResponse(): IResponse{
		return new Response($this->_encoder->jsonEncode($this->_ids));
	}
}