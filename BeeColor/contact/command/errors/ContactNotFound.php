<?php
namespace wfw\modules\BeeColor\contact\command\errors;

/**
 * La prise de contact n'a pas été trouvée
 */
final class ContactNotFound extends \Exception{}