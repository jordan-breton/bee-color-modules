<?php
namespace wfw\modules\BeeColor\contact\command\handlers;

use wfw\engine\core\command\ICommand;
use wfw\modules\BeeColor\contact\command\MarkContactsAsRead;
use wfw\modules\BeeColor\contact\domain\errors\MarkAsReadFailed;

/**
 * Marque les prises de contacts spécifiée comme luesg
 */
final class MarkContactsAsReadHandler extends ContactCommandHandler {
	/**
	 * Traite la commande
	 *
	 * @param ICommand $command Commande à traiter
	 */
	public function handleCommand(ICommand $command) {
		$res=[];
		/** @var MarkContactsAsRead $command */
		foreach($command->getIds() as $id){
			try{
				$contact = $this->get($id);
				$contact->markAsRead($command->getInitiatorId());
				$res[] = $contact;
			}catch(MarkAsReadFailed $e){}
		}
		$this->repos()->editAll($command,...$res);
	}
}