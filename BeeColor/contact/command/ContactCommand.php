<?php
namespace wfw\modules\BeeColor\contact\command;

use wfw\engine\core\command\Command;

/**
 * Commande concernant des prises de contact
 */
abstract class ContactCommand extends Command {}