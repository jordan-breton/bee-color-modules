<?php

use wfw\modules\BeeColor\contact\ContactDescriptor;
use wfw\engine\core\conf\WFW;

WFW::registerModules(ContactDescriptor::class);