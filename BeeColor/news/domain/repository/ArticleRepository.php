<?php
namespace wfw\modules\BeeColor\news\domain\repository;

use wfw\engine\core\command\ICommand;
use wfw\engine\core\domain\repository\IAggregateRootRepository;
use wfw\engine\lib\PHP\types\UUID;
use wfw\modules\BeeColor\news\domain\Article;

/**
 * Repository d'articles
 */
final class ArticleRepository implements IArticleRepository {
	/** @var IAggregateRootRepository $_repos */
	private $_repos;

	/**
	 * ArticleRepository constructor.
	 *
	 * @param IAggregateRootRepository $repos Repository
	 */
	public function __construct(IAggregateRootRepository $repos){
		$this->_repos = $repos;
	}

	/**
	 * Obtient l'article d'identifiant $id
	 *
	 * @param string $id
	 * @return null|Article
	 */
	public function get(string $id): ?Article {
		/** @var Article|null $article */
		$article = $this->_repos->getAggregateRoot(new UUID(UUID::V6, $id));
		return $article;
	}

	/**
	 * @param Article  $article Article à ajouter/modifier
	 * @param ICommand $command Commande ayant entraînée la création
	 */
	public function add(Article $article,ICommand $command): void{
		$this->_repos->addAggregateRoot($article, $command);
	}

	/**
	 * @param Article  $article Article à supprimer
	 * @param ICommand $command Commande ayant entraîné la modification
	 */
	public function edit(Article $article,ICommand $command): void{
		$this->_repos->modifyAggregateRoot($article, $command);
	}
	
	/**
	 * Retourne tous les articles correspondants aux identifiants
	 *
	 * @param string[] $ids Liste d'identifiants d'articles
	 * @return Article[]
	 */
	public function getAll(string... $ids): array {
		$uuids = [];
		foreach($ids as $id){$uuids[] = new UUID(UUID::V6,$id);}
		return $this->_repos->getAllAggregateRoots(...$uuids);
	}
}