<?php
namespace wfw\modules\BeeColor\news\domain\errors;

/**
 * L'archivage de l'article a échoué
 */
final class ArchivingFailed extends ArticleFailure {}