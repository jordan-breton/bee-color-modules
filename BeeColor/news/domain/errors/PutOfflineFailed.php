<?php
namespace wfw\modules\BeeColor\news\domain\errors;

/**
 * Levée lorsqu'un article ne peut pas être mis hors ligne.
 */
final class PutOfflineFailed extends ArticleFailure{}