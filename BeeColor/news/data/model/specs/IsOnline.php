<?php
namespace wfw\modules\BeeColor\news\data\model\specs;

use wfw\engine\core\data\specification\LeafSpecification;
use wfw\modules\BeeColor\news\data\model\objects\Article;

/**
 * L'article est en ligne
 */
final class IsOnline extends LeafSpecification {
	/**
	 *  Verifie que le candidat correspond à la spécification
	 *
	 * @param mixed $candidate Candidat à la specification
	 *
	 * @return bool
	 */
	public function isSatisfiedBy($candidate): bool {
		/** @var Article $candidate */
		return $candidate->isOnline();
	}
}