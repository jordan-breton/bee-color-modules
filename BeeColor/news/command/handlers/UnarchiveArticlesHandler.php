<?php
namespace wfw\modules\BeeColor\news\command\handlers;

use wfw\engine\core\command\ICommand;
use wfw\modules\BeeColor\news\command\UnarchiveArticles;
use wfw\modules\BeeColor\news\domain\errors\ArchivingFailed;

/**
 * Désarchive un article archivé.
 */
final class UnarchiveArticlesHandler extends ArticleCommandHandler {
	/**
	 * Traite la commande
	 *
	 * @param ICommand $command Commande à traiter
	 */
	public function handleCommand(ICommand $command) {
		/** @var UnarchiveArticles $command */
		foreach($command->getArticleIds() as $id){
			try{
				$article = $this->get($id);
				$article->unarchive($command->getInitiatorId());
				$this->repos()->edit($article,$command);
			}catch(ArchivingFailed $e){}
		}
	}
}