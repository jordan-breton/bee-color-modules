<?php
namespace wfw\modules\BeeColor\news\command\handlers;

use wfw\engine\core\command\ICommand;
use wfw\engine\lib\PHP\types\UUID;
use wfw\modules\BeeColor\news\command\CreateArticle;
use wfw\modules\BeeColor\news\domain\Article;
/**
 * Handler de commande de création d'article.
 */
final class CreateArticleHandler extends ArticleCommandHandler {
	/**
	 *  Traite la commande
	 * @param ICommand $command Commande à traiter
	 */
	public function handleCommand(ICommand $command) {
		/** @var CreateArticle $command */
		$article = new Article(
			new UUID(),
			$command->getTitle(),
			$command->getVisualLink(),
			$command->getContent(),
			$command->getInitiatorId(),
			$command->isOnline()
		);
		$this->repos()->add($article,$command);
	}
}