<?php
namespace wfw\modules\BeeColor\news\command;

use wfw\engine\core\command\Command;

/**
 * Commande de gestion des articles
 */
abstract class ArticleCommand extends Command {}