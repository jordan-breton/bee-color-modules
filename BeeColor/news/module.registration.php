<?php

use wfw\engine\core\conf\WFW;
use wfw\modules\BeeColor\news\NewsDescriptor;

WFW::registerModules(NewsDescriptor::class);