<?php
namespace wfw\modules\BeeColor\miel\lib\helper;

use wfw\engine\lib\HTML\resources\css\ICSSManager;
use wfw\engine\lib\HTML\resources\js\IJsScriptManager;

/**
 * Helper pour les vues.
 */
interface IMielHelper {
	/**
	 * @param string $key Clé à récupérer
	 * @return string attribut html à placer dans les balises.
	 */
	public function getHTMLForKey(string $key):string;
}